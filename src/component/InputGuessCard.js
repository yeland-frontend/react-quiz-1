import React from "react";

class InputGuessCard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.getInputCards = this.getInputCards.bind(this);
    this.toGuess = this.toGuess.bind(this);
  }

  render() {
    return (
      <div className="input-guess">
        <h1>Guess Card</h1>
        <input id="inputCards" onChange={this.getInputCards} type="text" value={this.props.guessCards}/>
        <br/>
        <button className="guess" onClick={this.toGuess}>Guess</button>
      </div>
    )
  }

  getInputCards(event) {
    const cards = event.target.value;
    this.props.getInput(cards);
  }

  toGuess(event) {
    event.preventDefault();
    this.props.getIsGuess(true);
  }

}

export default InputGuessCard;