import React from "react";
import "../show-area.less";

class ShowArea extends React.Component{
  render() {
    const showCards = this.props.show;
    return (
      <div className="show">
        <span>{showCards.substr(0,1)}</span>
        <span>{showCards.substr(1,1)}</span>
        <span>{showCards.substr(2,1)}</span>
        <span>{showCards.substr(3,1)}</span>
      </div>
    )
  }
}

export default ShowArea;