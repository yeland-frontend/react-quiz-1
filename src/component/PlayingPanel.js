import React from "react";
import InputGuessCard from "./InputGuessCard";
import ShowGuessCard from "./ShowGuessCard";
import "../playing-panel.less"

class PlayingPanel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      guessCards: '',
      isGuess: false,
      isSuccess: null
    };
    this.setGuessCards = this.setGuessCards.bind(this);
    this.setIsGuess = this.setIsGuess.bind(this);
    this.setIsSuccess = this.setIsSuccess.bind(this);
  }

  render() {
    return (
      <form className="playing-panel">
        <ShowGuessCard guessCards={this.state.guessCards} playResult={this.state.isSuccess}/>
        <InputGuessCard getInput={this.setGuessCards} getIsGuess={this.setIsGuess} guessCards={this.state.guessCards}/>
      </form>
    )
  }

  setGuessCards(guessCards) {
    this.setState({guessCards: guessCards});
    this.setState({isGuess: false});
  }

  setIsGuess(isGuess) {
    this.setState({isGuess: isGuess});
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.isGuess !== this.state.isGuess) {
      this.setIsSuccess(this.state.isGuess);
    }
    if (prevProps.resultCards !== this.props.resultCards) {
      this.setState({
        guessCards: '',
        isGuess: false,
        isSuccess: null
      })
    }
  }

  setIsSuccess(isGuess) {
    if (isGuess) {
      if (this.state.guessCards === this.props.resultCards) {
        this.setState({isSuccess: "SUCCESS"});
      } else {
        this.setState({isSuccess: "FAILED"});
      }
    } else {
      this.setState({isSuccess: null});
    }
  }
}

export default PlayingPanel;