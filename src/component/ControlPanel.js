import React from "react";
import ShowArea from "./ShowArea";
import "../control-panel.less";

class ControlPanel extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      showInTime: '',
      cards: null
    };
    this.createCards = this.createCards.bind(this);
    this.showResult = this.showResult.bind(this);
    this.timer = null;
  }

  render() {
    return (
      <form className="control-panel">
        <h1>New Card</h1>
        <button className="new" onClick={this.createCards}>New Card</button>
        <ShowArea show={this.state.showInTime}/>
        <button className="result" onClick={this.showResult}>Show Result</button>
      </form>
    )
  }

  createCards(event) {
    event.preventDefault();
    const cards = this.getRandomCards();
    this.props.getResultCards(cards);
    this.setState({
      showInTime: cards,
      cards: cards
    });
    this.showResult = this.showResult.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.cards !== this.state.cards) {
      this.timer = setTimeout(() => {
        this.setState({showInTime: ''})
      }, 3000);
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  getRandomCards() {
    let result = [];
    for (let i = 0; i < 4; i++) {
      const ranNum = Math.ceil(Math.random() * 25);
      result.push(String.fromCharCode(65 + ranNum));
    }
    return result.join('');
  }

  showResult(event) {
    event.preventDefault();
    this.setState({showInTime: this.props.resultCards});
  }
}

export default ControlPanel;