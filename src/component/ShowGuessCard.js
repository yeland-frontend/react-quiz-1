import React from "react";
import ShowArea from "./ShowArea";

class ShowGuessCard extends React.Component{
  render() {
    return(
      <div className="show-guess">
        <h1>Your Result</h1>
        <ShowArea show={this.props.guessCards}/>
        <p style={{color: (this.props.playResult === 'FAILED')? 'red' : 'green'}}>{this.props.playResult}</p>
      </div>
    )
  }
}

export default ShowGuessCard;