import React from "react";
import PlayingPanel from "./PlayingPanel";
import ControlPanel from "./ControlPanel";
import "../play.less";

class Play extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      resultCards: null
    };
    this.setResultCards = this.setResultCards.bind(this);
  }

  render() {
    return (
      <div className="play">
        <PlayingPanel resultCards={this.state.resultCards}/>
        <ControlPanel getResultCards={this.setResultCards} resultCards={this.state.resultCards}/>
      </div>
    )
  }

  setResultCards(cards) {
    this.setState({resultCards: cards});
  }
}

export default Play;